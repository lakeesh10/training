'use strict'
var orderDatabase = require('../dataAccess/orderDatabase.js');
var bundleDatabase = require('../dataAccess/bundleDatabase.js');
var userDatabase = require('../dataAccess/userDatabase.js');
var productDatabase = require('../dataAccess/productDatabase.js')
var findObject = require('./common.js').findObject;
var randomInt = require('./common.js').randomInt;

class OrderHandler {
    constructor() {

    }
    placeOrder(orderObject, reply) {
        orderDatabase.getOrderByOrderId().then((orders) => {

            orderObject.orderId = randomInt(0, 1000).toString();
            while (findObject(orders, orderObject.orderId))
                orderObject.orderId = randomInt(0, 1000).toString();

            userDatabase.getUser(orderObject.userId).then((user) => {
                orderObject.destination = user[0].location;

                productDatabase.getProduct(orderObject.productId).then((product) => {
                    orderObject.source = product[0].location;
                    orderObject.currentLocation = product[0].location;
                    orderObject.tracking = [];
                    orderObject.tracking.push(product[0].location);
                    orderObject.weight = product[0].weight;
                    orderObject.product = product[0];
                    orderDatabase.placeOrder(orderObject).then((res) => {
                        reply("Success");
                    }, (error) => {
                        reply("failure");
                    })
                }, (error) => {
                    reply("Product Not Found")
                })
            }, (error) => {
                reply("User Not Found")
            })


        }, (error) => {
            reply("failure1");
        })

    }
    trackOrderById(orderId, reply) {
        orderDatabase.getOrderByOrderId(orderId).then((order) => {
            reply(order[0]);
        }, (error) => {
            reply("Tracking Id does not exist").code(404);
        })
    }
    trackOrderByUser(userId, reply) {
        orderDatabase.getOrderByUserId(userId).then((orders) => {
            reply(orders);
        }, (error) => {
            reply("No Products Found")
        })
    }
    orderReached(userId, bundleId, reply) {
        userDatabase.getUser(userId).then((user) => {
            let location = user[0].location;
            orderDatabase.getOrderByBundleId(bundleId).then((bundle) => {
                let i = 0;
                for (i = 0; i < bundle.length; i++) {
                    bundle[i].bundleId = "-1";
                    bundle[i].currentLocation = location;
                    bundle[i].tracking.push(location);
                    orderDatabase.updateOrder(bundle[i]);
                }
                bundleDatabase.deleteBundle(bundleId).then((success) => {

                    reply("Success");
                }, (error) => {
                    reply("Cannot Delete")
                })
            }, (error) => {
                reply("Bundle Not Found")
            })
        }, (error) => {
            reply("User Not Found")
        })
    }
    addOrderToBundle(bundles, userId, toLocation, reply) {
        userDatabase.getUser(userId).then((user) => {
            let fromLocation = user[0].location;
            bundleDatabase.getBundle().then((result) => {
                let bundleId = randomInt(0, 1000).toString();
                while (findObject(result, bundleId))
                    bundleId = randomInt(0, 1000).toString();
                let bundleObject = {
                    'bundleId': bundleId,
                    'from': fromLocation,
                    'to': toLocation
                }
                let x = bundleDatabase.addBundle(bundleObject);
                let i = 0;
                for (i = 0; i < bundles.length; i++) {
                    orderDatabase.getOrderByOrderId(bundles[i]).then((bundle) => {
                        bundle[0].bundleId = bundleId;
                        bundle[0].currentLocation = "-1";
                        let y = orderDatabase.updateOrder(bundle[0]);
                    }, (error) => {
                        reply("Couldnt Add Bundle")
                    })
                }
                reply("Success");

            }, (error) => {
                reply("Cannot Access")
            })

        }, (error) => {
            reply("User Not Found")
        })

    }
    getBundlesReached(userId, reply) {
        userDatabase.getUser(userId).then((user) => {
            let location = user[0].location;
            orderDatabase.getOrderByCurrentLocation(location).then((bundles) => {
                reply(bundles)
            }, (error) => {
                reply("No Bundles Found")
            })
        }, (error) => {
            reply("UnAuthorised")
        })
    }
    getBundle(bundleId, reply) {

        orderDatabase.getOrderByBundleId(bundleId).then((bundles) => {
            reply(bundles)
        }, (error) => {
            reply("No Bundles Found")
        })
    }
    getBundlesTransit(userId, reply) {
        userDatabase.getUser(userId).then((user) => {
            let location = user[0].location;
            bundleDatabase.getBundleByTo(location).then((bundles) => {
                reply(bundles)
            }, (error) => {
                reply("No Bundles Found")
            })
        }, (error) => {
            reply("UnAuthorised")
        })
    }
    getBundlesTransported(userId, reply) {
        userDatabase.getUser(userId).then((user) => {
            let location = user[0].location;
            bundleDatabase.getBundleByFrom(location).then((bundles) => {
                reply(bundles)
            }, (error) => {
                reply("No Bundles Found")
            })
        }, (error) => {
            reply("UnAuthorised")
        })
    }
    getOrderForDelivery(userId, reply) {
        userDatabase.getUser(userId).then((user) => {
            let location = user[0].location;
            orderDatabase.getOrderForDelivery(location).then((orders) => {
                reply(orders);
            }, (error) => {
                reply("No Orders Out For Delivery")
            })
        }, (error) => {
            reply("User Not Found")
        })

    }
    deliverOrder(orderId, reply) {
        orderDatabase.getOrderByOrderId(orderId).then((order) => {
            if (order[0].currentLocation == order[0].destination) {
                let order1 = order[0]
                order1.currentLocation = "delivered"
                orderDatabase.updateOrder(order1).then((success) => {
                    reply("OrderDelivered");
                }, (error) => {
                    reply("Cannot Deliver")
                })
            }
            else {
                reply("Order In Transit");
            }
        }, (error) => {
            reply("User Not Found")
        })

    }

}
var orderHandler = new OrderHandler();

module.exports = orderHandler;