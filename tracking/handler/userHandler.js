'use strict'
var userDatabase = require('../dataAccess/userDatabase.js');
const server = require('../index.js')
var findObject = (objectArray, value) => {
    for (var j = 0; j < objectArray.length; j++) {

        if (objectArray[j].userId == value)
            return objectArray[j];
    }
    return null;
}

class UserHandler {
    constructor() {

    }
    registerUser(userObject, reply) {

        userDatabase.getUser().then((users) => {
            if (findObject(users, userObject.userId)) {
                reply("UserName already Exists").code(401);
            }
            else {
                userDatabase.registerUser(userObject).then((res) => {
                    if (userObject.isAdmin == "y")
                        reply("/admin/login")
                    else
                        reply("user/login")
                }, (err) => {
                    reply(err);
                })
            }

        }, (err) => {
            reply("Cannot Access").code(404);
        })

    }
    getUser(userId, reply) {
        userDatabase.getUser(userId).then((res) => {

            reply(res[0]);
        }, (err) => {
            reply(err);
        })
    }

    authoriseUser(request, userId, password, isAdmin, reply) {
        userDatabase.getUser(userId).then((result) => {
            console.log();
            if(! result[0])
                reply("UserName or Password you entered don't match.").code(401);
            if (result[0].password == password && result[0].isAdmin == isAdmin) {
                request.cookieAuth.set({ userId: userId });
                if (isAdmin == "y")
                    reply("/admin/home").code(200);
                else
                    reply("/user/home").code(200);
            }
            else
                reply("UserName or Password you entered don't match.").code(401);
        }, (error) => {
            reply("UserName or Password you entered don't match.").code(401)
        })

    }
}
let userHandler = new UserHandler();
module.exports = userHandler;