'use strict'
var productDatabase = require('../dataAccess/productDatabase.js');
var userDatabase = require('../dataAccess/userDatabase.js');
var findObject = (objectArray, value) => {
    for (var j = 0; j < objectArray.length; j++) {

        if (objectArray[j].productId == value)
            return objectArray[j];
    }
    return null;
}
class ProductHandler {
    constructor() {

    }
    addProduct(productObject, userId, reply) {
        userDatabase.getUser(userId).then((userObject) => {
            if (userObject[0].isAdmin != "y")
                reply("UnAuthorised").code(401);
            else {
                productObject.location = userObject[0].location;
                productDatabase.getProduct().then((products) => {
                    if (findObject(products, productObject.productId)) {
                        reply("ProductId already Exists").code(404);
                    }
                    else{
                    productDatabase.addProduct(productObject).then((res) => {
                        reply("success");
                    }, (err) => {
                        reply(err);
                    })
                    }
                },(err)=>{
                    reply("Cannot Access").code(404);
                })
                }

        },(error) => {
                reply("Cannot Access").code(204);
            })

    }

    getProducts(reply) {
        productDatabase.getProduct().then((res) => {
            reply(res);
        }, (err) => {
            reply(err);
        })

    }
    getProduct(productId, reply) {
        productDatabase.getProduct(productId).then((res) => {
            reply(res[0]);
        }, (err) => {
            reply(err);
        })

    }
}
let productHandler = new ProductHandler();
module.exports = productHandler;