'use strict'
var thinky = require('../dataAccess/thinky.js');
var type = thinky.type;

class Table {
    constructor() {

        this.userDetails = thinky.createModel("UserDetails", {
            userId: type.string(),
            name: type.string(),
            mobile: type.number().min(1000000000).max(9999999999).integer(),
            email: type.string().email(),
            address: {
                address: type.string(),
                city: type.string(),
                pincode: type.number().min(100000).max(999999).integer()
            },
            password: type.string().min(6),
            isAdmin: type.string().max(1),
            location: type.string().default(function () {
                return this.address.city;
            })
        },{
            pk: 'userId'
        });

        this.productDetails = thinky.createModel("ProductDetails", {
            productId: type.string(),
            productName: type.string(),
            location: type.string(),
            price: type.number(),
            description: type.string(),
            weight: type.number().integer().max(20)

        },{
            pk : 'productId'
        });
        this.bundleDetails = thinky.createModel("BundleDetails",{
            bundleId : type.string(),
            from : type.string(),
            to : type.string()
        },{
            pk: 'bundleId'
        });
        this.orderDetails = thinky.createModel("OrderDetails",{
            bundleId : type.string().optional().default("-1"),
            orderId : type.string(),
            productId : type.string(),
            userId : type.string(),
            source : type.string(),
            destination : type.string(),
            currentLocation: type.string().optional().default("-1"),
            tracking : type.array().optional().default([]),
            weight : type.number().integer().max(20)
        },{
            pk: 'orderId'
        })


    }
}
var tableInstance = new Table();
module.exports = tableInstance;