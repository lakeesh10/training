'use strict'
var thinky = require('../dataAccess/thinky.js')
var co = require('co');
var UserDetails = require('./table.js').userDetails;
var r=thinky.r;


class UserDatabase {
    constructor() {

    }

    registerUser(userObject) {
        return co(function* () {
            try {
                return yield UserDetails.save(userObject);
            }
            catch (err) {
                throw err;

            }
        })

    }
    getUser(userId) {
        return co(function* () {
            try {
                if (!userId) 
                    return yield r.table('UserDetails'); 
                else {
                    return yield r.table('UserDetails').filter({'userId':userId});
                }
            }
            catch (err) {
                throw err;

            }
            
        })
    }

    deleteUser(userId) {
        return co(function* () {
            try {
                    return yield r.table('UserDetails').filter({'userId':userId}).delete();
            }
            catch (err) {
                throw err;

            }
            
        })
    }
}
let userData = new UserDatabase();
module.exports = userData;