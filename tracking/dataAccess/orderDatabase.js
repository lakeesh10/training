'use strict'
var thinky = require('../dataAccess/thinky.js')
var co = require('co');
var OrderDetails = require('./table.js').orderDetails;
var BundleDetails = require('./table.js').bundleDetails;
var r = thinky.r;

class OrderDatabase{
    constructor(){}
    placeOrder(orderObject){
        return co(function* () {
            try {
                return yield OrderDetails.save(orderObject);
            }
            catch (err) {
                throw err;

            }
        })
    }
    getOrderByOrderId(orderId){
        return co(function *(){
            try{
                if(!orderId)
                    return yield r.table('OrderDetails')
                let order = yield r.table('OrderDetails').filter({'orderId':orderId});
                    return order
            }
            catch(error){
                throw error;
            }
        })
            
    }
    getOrderByCurrentLocation(location){
        return co(function *(){
            try{
                let order = yield r.table('OrderDetails').filter({'currentLocation':location});
                    return order
            }
            catch(error){
                throw error;
            }
        })
            
    }
    getOrderForDelivery(location){
        return co(function *(){
            try{
                let order = yield r.table('OrderDetails').filter({'destination':location, 'currentLocation' : location});
                    return order
            }
            catch(error){
                throw error;
            }
        })
            
    }
    getOrderByBundleId(bundleId){
        return co(function *(){
            try{
                let order = yield r.table('OrderDetails').filter({'bundleId':bundleId});
                    return order
            }
            catch(error){
                throw error;
            }
        })
            
    }
    getOrderByUserId(userId){
        return co(function *(){
            try{
                let order = yield r.table('OrderDetails').filter({'userId':userId});
                    return order
            }
            catch(error){
                throw error;
            }
        })
            
    }
    updateOrder(orderObject){
        return co(function * (){
            try{
                //let order = yield OrderDetails.get({ 'orderId': orderId });
                return yield r.table("OrderDetails").filter({'orderId':orderObject.orderId}).update(orderObject);
                    

            }
            catch(error){
                throw error;
            }
        })
    }
    deleteOrder(orderId){
        return co(function * (){
            try{
                return yield r.table("OrderDetails").filter({ 'orderId': orderId }).delete();
            }
            catch(error){
                throw error;
            }
        })
    }

    
}
var orderDatabase = new OrderDatabase()

module.exports = orderDatabase;