'use strict'
var thinky = require('../dataAccess/thinky.js')
var co = require('co');
var OrderDetails = require('./table.js').orderDetails;
var BundleDetails = require('./table.js').bundleDetails;
var r= thinky.r;


class BundleDatabase{
    constructor(){

    }
    addBundle(bundleObject){
        return co(function* () {
            try {
                return yield BundleDetails.save(bundleObject);
            }
            catch (err) {
                throw err;

            }
        })
    }
    getBundle(bundleId){
        return co(function *(){
            try{
                if(!bundleId)
                    return yield r.table('BundleDetails');
                
            }
            catch(error){
                throw error;
            }
        })
            
    }
    getBundleByFrom(fromLocation){
        return co(function *(){
            try{
                let bundle = yield r.table('BundleDetails').filter({'from':fromLocation});
                    return bundle
            }
            catch(error){
                throw error;
            }
        })
            
    }
    getBundleByTo(toLocation){
        return co(function *(){
            try{
                let bundle = yield r.table('BundleDetails').filter({ 'to': toLocation });
                    return bundle
            }
            catch(error){
                throw error;
            }
        })
            
    }
    deleteBundle(bundleId){
        return co(function * (){
            try{
                return yield r.table("BundleDetails").filter({ 'bundleId': bundleId }).delete();
            }
            catch(error){
                throw error;
            }
        })
    }

}

let bundleDatabase =new BundleDatabase();
module.exports = bundleDatabase;