'use strict'
var thinky = require('../dataAccess/thinky.js')
var co = require('co');
var productDetails = require('./table.js').productDetails;
var r= thinky.r;


class ProductDatabase {
    constructor() {

    }

    addProduct(productObject) {
        return co(function* () {
            try {
                let x = yield productDetails.save(productObject);
                return "Sucess"
            }
            catch (err) {
                throw err;

            }
        })

    }
    getProduct(productId) {
        return co(function* () {
            try {
                if (!productId) 
                    return yield r.table('ProductDetails'); 
                else {
                    let product = yield r.table('ProductDetails').filter({'productId':productId});
                    return yield product
                }
            }
            catch (err) {
                throw err;

            }
            
        })

    }

    deleteProduct(productId) {
        return co(function* () {
            try {
                if (!productId) 
                    return yield r.table('ProductDetails').delete(); 
                else {
                    let product = yield r.table('ProductDetails').filter({'productId':productId}).delete();
                    return yield product
                }
            }
            catch (err) {
                throw err;

            }
            
        })

    }
}
var productDatabase = new ProductDatabase();
module.exports = productDatabase;