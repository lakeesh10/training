var joi = require('joi');
var location = ["chennai", "bangalore", "mumbai", "hyderabad", "pune", "kolkata", "delhi"]
const loginSchema = joi.object({
  'userId': joi.string().required(),
  'password': joi.string().required(),
  'isAdmin' : joi.string().valid(['y', 'n']).required()
});

const addProductSchema = joi.object({
  'productId': joi.string().required(),
  'productName': joi.string().required(),
  'description': joi.string().required(),
  'price': joi.number().min(0).max(60000).required(),
  'weight': joi.number().min(0).max(20).required(),
});

const registrationSchema = joi.object({
  'userId': joi.string().required(),
  'name': joi.string().required(),
  'mobile': joi.number().integer().min(1000000000).max(9999999999).required(),
  'email': joi.string().email().required(),
  'address':{
    'address' : joi.string().required(),
    'city' : joi.string().valid(location).required(),
    'pincode': joi.number().min(100000).max(999999).integer()
  },
  'password': joi.string().min(6).required(),
  'retypePassword' : joi.string().min(6).required(),
  'isAdmin' : joi.string().valid(['y', 'n']).required()
});


const productId = joi.object({
  'productId': joi.string().required(),
});

const orderId = joi.object({
  'orderId': joi.string().required(),
});
const bundleId = joi.object({
  'bundleId': joi.string().required(),
});

const paramId = joi.object({
  'snippetId': joi.string().required()
});
const addToBundleSchema = joi.object({
  'bundles': joi.array().items(joi.string().required()),
  'toLocation' : joi.string().valid(location).required()
});
module.exports = {
  loginSchema,
  addProductSchema,
  productId,
  orderId,
  bundleId,
  addToBundleSchema,
  registrationSchema
 
};