'use strict'
const assert = require('assert');
const sinon = require('sinon');
var productDatabase = require('../../dataAccess/productDatabase.js');

const add1 = {
    "productId": "123121",
    "productName": "s1",
    "price": 1234,
    "weight": 3,
    "description": "This is product s1",
    "location": "Chennai"
}

describe('Testing productDatabase', function () {
    it('addProduct', function (done) {
        this.timeout(15000);
        productDatabase.addProduct(add1).then(function(res) {
            console.log(res);
            assert.equal(res, "Sucess");
            done()
        }, function(error){
            console.log("hhjj");
            done(error); 
        })
    })
})