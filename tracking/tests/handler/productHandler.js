'use strict'
const assert = require('chai').assert;
const sinon = require('sinon');
var productHandler = require('../../handler/productHandler.js');
var productDatabase= require('../../dataAccess/productDatabase.js');

const add1 ={
    "productId" : "1234",
    "productName" : "s1",
    "price" : 1234,
    "weight" : 3,
    "description" : "This is product s1"
}
 
describe('Testing productHandler', () => {
    
after(()=>{
    productDatabase.deleteProduct("1234");
})

    it('addProduct', function(done) {
        this.timeout(5000)
        let reply = function(res){
            let _code;
            console.log(res)
            assert.equal(res ,"success");
            done()
            return {
                code: function(inp){
                    _code = inp;
                }
            }
        }
        productHandler.addProduct(add1, "adminBangalore", reply)
    })
    it('addProduct', function(done) {
        this.timeout(5000)
        let reply = function(res){
            let _code;
            console.log(res)
            assert.equal(res ,"ProductId already Exists");
            done()
            return {
                code: function(inp){
                    _code = inp;
                }
            }
        }
        productHandler.addProduct(add1, "adminBangalore", reply)
    })
    it('addProduct', function(done) {
        this.timeout(5000)
        let reply = function(res){
            let _code;
            console.log(res)
            assert.equal(res ,"UnAuthorised");
            done()
            return {
                code: function(inp){
                    _code = inp;
                }
            }
        }
        productHandler.addProduct(add1, "userBangalore", reply)
    })
    it('getProduct', function(done) {
        this.timeout(5000)
        let reply = function(res){
            let _code;
            console.log(res)
            assert.equal(res.productName ,"s1");
            done()
            return {
                code: function(inp){
                    _code = inp;
                }
            }
        }
        productHandler.getProduct("1234", reply)
    })
    it('getProducts', function(done) {
        this.timeout(5000)
        let reply = function(res){
            let _code;
            console.log(res)
            console.log(res.length)
            assert.equal(res.length ,2);
            done()
            return {
                code: function(inp){
                    _code = inp;
                }
            }
        }
        productHandler.getProducts(reply)
    })
               
})