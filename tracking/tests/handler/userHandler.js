'use strict'
const assert = require('chai').assert;
const sinon = require('sinon');
var userHandler = require('../../handler/userHandler.js');
var userDatabase= require('../../dataAccess/userDatabase.js');

const add1 ={
"userId": "1",
            "name": "lakeesh",
            "mobile": 8056249067,
            "email": "lak@gmail.com",
            "address": {
                "address":"jaksdk",
                "city": "chennai",
                "pincode": 600089
            },
            "password": "1234567",
            "isAdmin": "y"
            }
 
describe('Testing orderHandler', () => {
    
after(()=>{
    userDatabase.deleteUser("1");
})

    it('addUser', function(done) {
        this.timeout(5000)
        let reply = function(res){
            let _code;
            console.log(res)
            assert.equal(res ,"/admin/login");
            done()
            return {
                code: function(inp){
                    _code = inp;
                }
            }
        }
        userHandler.registerUser(add1, reply)
    })
    it('addUser', function(done) {
        this.timeout(5000)
        let reply = function(res){
            let _code;
            console.log(res)
            assert.equal(res ,"UserName already Exists");
            done()
            return {
                code: function(inp){
                    _code = inp;
                }
            }
        }
        userHandler.registerUser(add1, reply)
    })
    it('getUser', function(done) {
        this.timeout(5000)
        let reply = function(res){
            let _code;
            console.log(res)
            assert.equal(res.name ,"lakeesh");
            done()
            return {
                code: function(inp){
                    _code = inp;
                }
            }
        }
        userHandler.getUser("1", reply)
    })
    it('authoriseUser', function(done) {
        this.timeout(5000)
        let reply = function(res){
            let _code;
            console.log(res)
            assert.equal(res,"/admin/home");
            done()
            return {
                code: function(inp){
                    _code = inp;
                }
            }
        }
         let request={}
        request.cookieAuth={
            'set' : function(x){return x;}
        }
        
        userHandler.authoriseUser(request,"adminChennai","1234567","y", reply)
    })
    it('authoriseUserFails', function(done) {
        this.timeout(5000)
        let reply = function(res){
            let _code;
            console.log(res)
            assert.equal(res,"UserName or Password you entered don't match.");
            done()
            return {
                code: function(inp){
                    _code = inp;
                }
            }
        }
        var x=""
        let request={}
        request.cookieAuth={
            'set' : function(x){return x;}
        }
        
        console.log(request)
        userHandler.authoriseUser(request,"1","123456","y", reply)
    })
    it('authoriseUserFails', function(done) {
        this.timeout(5000)
        let reply = function(res){
            let _code;
            console.log(res)
            assert.equal(res,"UserName or Password you entered don't match.");
            done()
            return {
                code: function(inp){
                    _code = inp;
                }
            }
        }
        var x=""
        let request={}
        request.cookieAuth={}
        request.cookieAuth.set=function(user){
            x=user;
            console.log(user);
        }
        userHandler.authoriseUser(request,"1212","123456","y", reply)
    })
})