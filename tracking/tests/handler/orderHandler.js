'use strict'
const assert = require('chai').assert;
const sinon = require('sinon');
var orderHandler = require('../../handler/orderHandler.js');
var orderDatabase = require('../../dataAccess/orderDatabase.js');

const add1 =
    {
        "productId": "productMumbai",
        "userId": "userPune"
    }


describe('Testing orderHandler', () => {


    before((done) => {
        let order = {
            "bundleId": "-1",
            "orderId": "12345",
            "productId": "productMumbai",
            "userId": "userChennai",
            "source": "mumbai",
            "destination": "chennai",
            "tracking": ["mumbai"],
            "weight": 10
        }
        orderDatabase.placeOrder(order).then((res) => {
            done();
        }, (err) => {
            throw err;
        })

    })

    it('placeOrder', function (done) {
        this.timeout(5000)
        let reply = function (res) {
            let _code;
            assert.equal(res, "Success");
            done()
            return {
                code: function (inp) {
                    _code = inp;
                }
            }
        }
        orderHandler.placeOrder(add1, reply)
    })
    it('trackOrderById', function (done) {
        this.timeout(5000)
        let reply = function (res) {
            let _code;
            assert.equal(res.productId, "productMumbai");
            done()
            return {
                code: function (inp) {
                    _code = inp;
                }
            }
        }
        orderHandler.trackOrderById("12345", reply)
    })

    it('trackOrderByUser', function (done) {
        this.timeout(5000)
        let reply = function (res) {
            let _code;
            assert.equal(res.length, 0);
            done()
            return {
                code: function (inp) {
                    _code = inp;
                }
            }
        }
        orderHandler.trackOrderByUser("userBangalore", reply)
    })

    it('getBundlesReached', function (done) {
        this.timeout(5000)
        let reply = function (res) {
            let _code;
            console.log(res)
            assert.equal(res.length, 0);
            done()
            return {
                code: function (inp) {
                    _code = inp;
                }
            }
        }
        orderHandler.getBundlesReached("adminChennai", reply)
    })





    it('addOrderToBundle', function (done) {
        this.timeout(5000)
        let reply = function (res) {
            let _code;
            assert.equal(res, "Success");
            done()
            return {
                code: function (inp) {
                    _code = inp;
                }
            }
        }
        let bundles = ["12345"]
        orderHandler.addOrderToBundle(bundles, "adminMumbai", "chennai", reply)
    })
    it('getBundlesTransit', function (done) {
        this.timeout(5000)
        let reply = function (res) {
            let _code;
            console.log(res)
            assert.equal(res.length, 1);
            done()
            return {
                code: function (inp) {
                    _code = inp;
                }
            }
        }
        orderHandler.getBundlesTransit("adminChennai", reply)
    })
    it('getBundlesTransported', function (done) {
        this.timeout(5000)
        let reply = function (res) {
            let _code;
            assert.equal(res.length, 1);
            done()
            return {
                code: function (inp) {
                    _code = inp;
                }
            }
        }
        orderHandler.getBundlesTransported("adminMumbai", reply)
    })

})
describe('Testing orderHandler1', () => {
    let bundleId = ""
    let orderId = ""
    after(() => {
        orderDatabase.deleteOrder("12345");
        orderDatabase.deleteOrder(orderId);
    })
    before((done) => {
        orderDatabase.getOrderByOrderId("12345").then((res) => {
            bundleId = res[0].bundleId;
            console.log("order 12345", res);
            orderDatabase.getOrderByUserId("userPune").then((res) => {
                orderId = res[0].orderId;
                console.log(res[0])
                console.log("OrderId", orderId)
                done();
            }, (err) => {
                throw err;
            })

        }, (err) => {
            throw err;
        })
    })
    it('getBundle', function (done) {
        this.timeout(5000)
        let reply = function (res) {
            let _code;
            console.log(res)
            console.log(res.length)
            assert.equal(res.length, 1);
            done()
            return {
                code: function (inp) {
                    _code = inp;
                }
            }
        }
        console.log("bundleId : ", bundleId)
        orderHandler.getBundle(bundleId, reply)
    })
    it('orderReached', function (done) {
        this.timeout(5000)
        let reply = function (res) {
            let _code;
            console.log(res)
            console.log(res.length)
            assert.equal(res, "Success");
            done()
            return {
                code: function (inp) {
                    _code = inp;
                }
            }
        }
        orderHandler.orderReached("adminChennai", bundleId, reply)
    })
    it('getOrderForDelivery', function (done) {
        this.timeout(5000)
        let reply = function (res) {
            let _code;
            console.log(res)
            console.log(res.length)
            assert.equal(res.length, 1);
            done()
            return {
                code: function (inp) {
                    _code = inp;
                }
            }
        }
        orderHandler.getOrderForDelivery("adminChennai", reply)
    })
    it('orderDelivery', function (done) {
        this.timeout(5000)
        let reply = function (res) {
            let _code;
            console.log(res)
            console.log(res.length)
            assert.equal(res, "OrderDelivered");
            done()
            return {
                code: function (inp) {
                    _code = inp;
                }
            }
        }
        orderHandler.deliverOrder("12345", reply)
    })
    it('orderDeliveryFails', function (done) {
        this.timeout(5000)
        let reply = function (res) {
            let _code;
            console.log(res)
            assert.equal(res, "Order In Transit");
            done()
            return {
                code: function (inp) {
                    _code = inp;
                }
            }
        }
        console.log(orderId)
        orderHandler.deliverOrder(orderId, reply)
    })


})

