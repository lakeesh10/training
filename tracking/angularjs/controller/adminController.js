app.controller("bundleTransitController", function ($scope, $http, $location, $rootScope, $routeParams, $route) {

    $scope.page = {};
    var req = {
        method: 'GET',
        url: 'http://tracking.in:3000/admin/getTransit',

    }
    console.log(req);
    $http(req).then(function successCallback(response) {
        $scope.bundles = response.data;
        console.log($scope.bundles)


    }, function errorCallback(response) {
        $scope.errorStatus = response.status;
        console.log(response.status);
    })
    $scope.received = function (bundleId) {
        console.log(bundleId);
        var req1 = {
            method: 'POST',
            url: 'http://tracking.in:3000/admin/reached',
            data: { bundleId: bundleId }


        }
        console.log(req1);
        $http(req1).then(function successCallback(response) {
            $route.reload();

        }, function errorCallback(response) {
            $scope.errorStatus = response.status;
            console.log(response.status);
        })

    }
    $scope.showPrompt = function(bundleId) {
   $scope.currentBundle = bundleId;
   var req2 = {
            method: 'POST',
            url: 'http://tracking.in:3000/admin/getBundle',
            data: { bundleId: bundleId }
        }
        $http(req2).then(function successCallback(response) {
                $scope.openBundle = response.data;
                console.log($scope.openBundle)


            }, function errorCallback(response) {
                $scope.errorStatus = response.status;
                console.log(response.status);
            })
  };

});
app.controller("bundleReceivedController", function ($scope, $http, $location, $rootScope, $routeParams, ngToast, $route) {
    $scope.bundleWeight = 0;
    $scope.addBundles = {}
    $scope.addBundles.bundles = [];
    $scope.addBundles.toLocation = "Location";
    $scope.bundle = [];
    console.log($scope.addBundles.location);
    $scope.location = ["chennai", "bangalore", "mumbai", "hyderabad", "pune", "kolkata", "delhi"]
    var req = {
        method: 'GET',
        url: 'http://tracking.in:3000/admin/getRecieved',

    }
    console.log(req);
    $http(req).then(function successCallback(response) {
        $scope.orders = response.data;
        console.log($scope.orders)
        for (var i = 0; i < $scope.orders.length; i++) {
            var obj = $scope.orders[i];
            
            if (obj.currentLocation == obj.destination) {
                $scope.orders.splice(i, 1);
                i--;
            }
        }
        if ($scope.orders)
            if ($scope.location.indexOf($scope.orders[0].currentLocation) !== -1)
                $scope.location.splice($scope.location.indexOf($scope.orders[0].currentLocation), 1);
        console.log($scope.location)

    }, function errorCallback(response) {
        $scope.errorStatus = response.status;
        console.log(response.status);
    })

    console.log($scope.addBundles)

    $scope.addToBundle = function (order) {
        if ($scope.bundleWeight + order.weight > 40) {
            console.log("Too weight");
            $scope.error = "Bundle can carry maximum 40kg only"
        }
        else {
            $scope.bundleWeight = $scope.bundleWeight + order.weight;
            $scope.bundle.push(order);
            $scope.addBundles.bundles.push(order.orderId)
            for (i = 0; i < $scope.orders.length; i++) {
                if ($scope.orders[i].orderId == order.orderId) {
                    $scope.orders.splice(i, 1);
                }
            }
        }
    }

    $scope.changeLocation = function (x) {
        $scope.addBundles.toLocation = x;
    }

    $scope.removeFromBundle = function (order) {
        $scope.bundleWeight = $scope.bundleWeight - order.weight;
        for (i = 0; i < $scope.bundle.length; i++) {
            if ($scope.bundle[i].orderId == order.orderId) {
                $scope.bundle.splice(i, 1);
            }
        }
        $scope.addBundles.bundles.splice($scope.addBundles.bundles.indexOf(order.orderId), 1);
        $scope.orders.push(order);
        console.log($scope.orders, $scope.bundle, $scope.bundleId)

    }
    $scope.addBundle = function () {
        console.log($scope.addBundles);
        if($scope.addBundles.toLocation == "Location"){
            $scope.error = "Location is invalid";
            return;
        }
        var req1 = {
            method: 'POST',
            url: 'http://tracking.in:3000/admin/addBundle',
            data: $scope.addBundles


        }
        console.log(req1);
        $http(req1).then(function successCallback(response) {
            $http(req).then(function successCallback(response) {
                console.log(response.data)
                $scope.bundle = [];
                $scope.addBundles = {};
                $scope.addBundles.toLocation = "Lcoation"
                console.log($scope.bundle, $scope.addBundles)
                ngToast.create('Bundle is Transported Successfully');
                $route.reload();

            }, function errorCallback(response) {
                $scope.errorStatus = response.status;
                console.log(response.status);

            })
            console.log(response.data)


        }, function errorCallback(response) {
            if(response.status == 400){
                console.log(response.status);
                var start_pos = response.data.message.indexOf('[') + 1;
                var end_pos = response.data.message.indexOf(']',start_pos);
                var text_to_get = response.data.message.substring(start_pos,end_pos)
                console.log(text_to_get);
                $scope.error = "";
                $scope.error = text_to_get;
                console.log(response.data.validation.keys[0]);
            }
            if(response.status == 401){
                console.log(response.data);
                $scope.error = response.data;
            }
        })

    }

});
app.controller("bundleTransportedController", function ($scope, $http, $location, $rootScope, $routeParams) {

    $scope.page = {};
    var req = {
        method: 'GET',
        url: 'http://tracking.in:3000/admin/getTransported',

    }
    console.log(req);
    $http(req).then(function successCallback(response) {
        $scope.bundles = response.data;
        console.log($scope.bundles)


    }, function errorCallback(response) {
        $scope.errorStatus = response.status;
        console.log(response.status);
    })

    $scope.showPrompt = function(bundleId) {
   $scope.currentBundle = bundleId;
   var req2 = {
            method: 'POST',
            url: 'http://tracking.in:3000/admin/getBundle',
            data: { bundleId: bundleId }
        }
        $http(req2).then(function successCallback(response) {
                $scope.openBundle = response.data;
                console.log($scope.openBundle)


            }, function errorCallback(response) {
                $scope.errorStatus = response.status;
                console.log(response.status);
            })
  };

});
app.controller("deliveryController", function ($scope, $http, $location, $rootScope, $routeParams, $route,ngToast) {

    $scope.page = {};
    var req = {
        method: 'GET',
        url: 'http://tracking.in:3000/admin/getDelivered',

    }
    console.log(req);
    $http(req).then(function successCallback(response) {
        $scope.orders = response.data;
        


    }, function errorCallback(response) {
        $scope.errorStatus = response.status;
        console.log(response.status);
    })
    $scope.deliver = function (orderId) {
        console.log(orderId);
        var req1 = {
            method: 'PUT',
            url: 'http://tracking.in:3000/admin/deliver',
            data: { orderId: orderId }


        }
        console.log(req1);
        $http(req1).then(function successCallback(response) {
            ngToast.create('Product is Delivered');
            $route.reload()
            console.log(response.data)



        }, function errorCallback(response) {
            $scope.errorStatus = response.status;
            console.log(response.status);
        })

    }
});
app.controller("addItemController", function ($scope,$route, $http, $location, $rootScope, $routeParams, ngToast) {

    $scope.product = {};
    $scope.addItem = function () {
        $scope.error = "";
        $scope.product.weight = parseInt($scope.product.weight);
        $scope.product.price = parseInt($scope.product.price)
        console.log($scope.product);
        var req = {
            method: 'POST',
            url: 'http://tracking.in:3000/admin/addProduct',
            data: $scope.product
        }
        console.log(req);
        $http(req).then(function successCallback(response) {
            $scope.response = response.data;
            console.log($scope.response)
            ngToast.create('Item is Added');
            $route.reload()
        }, function errorCallback(response) {
            if(response.status == 400){
                console.log(response.status);
                var start_pos = response.data.message.indexOf('[') + 1;
                var end_pos = response.data.message.indexOf(']',start_pos);
                var text_to_get = response.data.message.substring(start_pos,end_pos)
                console.log(text_to_get);
                $scope.error = "";
                $scope.error = text_to_get;
                console.log(response.data.validation.keys[0]);
            }
            if(response.status == 404){
                console.log(response.data);
                $scope.error = response.data;
            }
            if(response.status == 401){
                console.log(response.data);
                $scope.error = response.data.error;
            }
        })

    }
});