var app = angular.module('trackingApp', ['ngToast','datatables', 'ngRoute']);
app.config(function ($routeProvider, $httpProvider) {
    $httpProvider.defaults.withCredentials = true;
    $routeProvider.
        when("/", {
            templateUrl: "./template/welcome.html"
        })
        .when("/:param/login", {
            templateUrl: "./template/login.html"
        })
        .when("/:param/register", {
            templateUrl: "./template/register.html"
        })
        .when("/user/home", {
            templateUrl: "./template/userHome.html"
        })
        .when("/admin/home", {
            templateUrl: "./template/adminHome.html"
        })
        .when("/admin/bundleReached", {
            templateUrl: "./template/bundleReached.html"
        })
        .when("/admin/bundleTransported", {
            templateUrl: "./template/bundleTransported.html"
        })
        .when("/admin/delivery", {
            templateUrl: "./template/delivery.html"
        })
        .when("/admin/addItem", {
            templateUrl: "./template/addItem.html"
        })
        .when("/user/trackOrder", {
            templateUrl: "./template/tracking.html"
        })
        .when("/user/logout", {
            controller : "logoutController"
        })
        .when("/user/continueShopping", {
            templateUrl : "/template/continueShopping.html"
        })
        .when("/user/placeOrder/:productId", {
            templateUrl: "./template/placeOrder.html"
        })
        .otherwise("/", {
            templateUrl: "./template/welcome.html",
            
        })
});
app.config(['ngToastProvider', function(ngToastProvider) {
  ngToastProvider.configure({
    animation: 'slide',
    verticalPosition: 'bottom',
      horizontalPosition: 'center', // or 'fade'
  });
}]);

app.controller("MainController", function ($scope, $http, $location, $rootScope) {
    $scope.page = {};

});
app.controller("registerController", function ($scope, $http, $location, $rootScope, $routeParams, ngToast) {
    console.log($routeParams.param);

    $scope.page = {};
    $scope.page.form = {};
    $scope.page.form.address = {};
    $scope.page.form.address.city = "Select City"
    $scope.location = ["chennai", "bangalore", "mumbai", "hyderabad", "pune", "kolkata", "delhi"]
    if ($routeParams.param == "admin")
        $scope.page.form.isAdmin = "y";
    else
        $scope.page.form.isAdmin = "n";

    $scope.changeLocation = function(x){
         $scope.page.form.address.city =x;
    }
    $scope.page.form.register = function () {
        $scope.error = "";
        $scope.page.form.mobile = parseInt($scope.page.form.mobile);
        $scope.page.form.address.pincode = parseInt($scope.page.form.address.pincode)
        console.log($scope.page.form);
        var req = {
            method: 'POST',
            url: 'http://tracking.in:3000/user/register',
            data: $scope.page.form
        }
        console.log(req);
        if($scope.page.form.password != $scope.page.form.retypePassword){
            $scope.error = "Retype password is not the same as password";
            return;
        }
        if($scope.page.form.address.address == null){
            $scope.error = "Address field is empty";
            return;
        }
        if($scope.page.form.userId == null){
            $scope.error = "UserName is required";
            return;
        }
        if($scope.page.form.address.city == "Select City"){
            $scope.error = "City is invalid";
            return;
        }
        if(!$scope.page.form.mobile  || $scope.page.form.mobile < 1000000000 || $scope.page.form.mobile >9999999999){
            $scope.error = "Invalid Mobile Number";
            return;
        }
        if(!$scope.page.form.address.pincode || $scope.page.form.address.pincode < 100000 || $scope.page.form.address.pincode >999999){
            $scope.error = "Invalid PinCode";
            return;
        }

        $http(req).then(function successCallback(response) {
            $scope.response = response.data;
            console.log($scope.response)
            $location.path(response.data)
        }, function errorCallback(response) {
            if(response.status == 400){
                console.log(response.status);
                var start_pos = response.data.message.indexOf('[') + 1;
                var end_pos = response.data.message.indexOf(']',start_pos);
                var text_to_get = response.data.message.substring(start_pos,end_pos)
                console.log(text_to_get);
                $scope.error = "";
                $scope.error = text_to_get;
                console.log(response.data.validation.keys[0]);
            }
            if(response.status == 401){
                console.log(response.data);
                $scope.error = response.data;
            }

        })
        
            

    }
    $scope.page.form.login = function () {
        console.log($routeParams.param);
        ngToast.create('Successfully Registered');
        $location.path("/" + $routeParams.param + "/login")

    }
});

app.controller("loginController", function ($scope, $http, $location, $rootScope, $routeParams, ngToast) {
    console.log($routeParams.param);
    $scope.page = {};
    $scope.page.login = {};
    if ($routeParams.param == "admin")
        $scope.page.login.isAdmin = "y";
    else
        $scope.page.login.isAdmin = "n";

     var req = {
            method: 'GET',
            url: 'http://tracking.in:3000/user/logout',
            
        }
        console.log(req);
        $http(req).then(function successCallback(response) {
            $scope.response = response.data;
            console.log($scope.response)
            
        }, function errorCallback(response) {
            $scope.errorStatus = response.status;
            console.log(response.status);
        })
    $scope.page.login.login = function () {
        console.log($scope.page.login);
        var req = {
            method: 'POST',
            url: 'http://tracking.in:3000/user/login',
            data: $scope.page.login
        }
        console.log(req);
        $http(req).then(function successCallback(response) {
            $scope.response = response.data;
            console.log($scope.response)
            ngToast.create('Logged in successfully');
                $location.path(response.data);
        }, function errorCallback(response) {
            if(response.status == 400){
                console.log(response.status);
                var start_pos = response.data.message.indexOf('[') + 1;
                var end_pos = response.data.message.indexOf(']',start_pos);
                var text_to_get = response.data.message.substring(start_pos,end_pos)
                console.log(text_to_get);
                $scope.error = "";
                $scope.error = text_to_get;
                console.log(response.data.validation.keys[0]);
            }
            if(response.status == 401){
                console.log(response.data);
                $scope.error = response.data;
            }
        })

    }
    $scope.page.login.register = function () {
        console.log($routeParams.param);
        $location.path("/" + $routeParams.param + "/register")

    }
});
app.controller("logoutController", function ($scope, $http, $location, $rootScope, $routeParams) {
   
        var req = {
            method: 'GET',
            url: 'http://tracking.in:3000/user/logout',
            
        }
        console.log(req);
        $http(req).then(function successCallback(response) {
            $scope.response = response.data;
            console.log($scope.response)
            
                $location.path("/");
            
        }, function errorCallback(response) {
            $scope.errorStatus = response.status;
            console.log(response.status);
        })

    
    
});
app.controller("welcomeController", function ($scope, $http, $location, $rootScope, $routeParams) {
   
        var req = {
            method: 'GET',
            url: 'http://tracking.in:3000/user/logout',
            
        }
        console.log(req);
        $http(req).then(function successCallback(response) {
            $scope.response = response.data;
            console.log($scope.response)
            
                
            
        }, function errorCallback(response) {
            $scope.errorStatus = response.status;
            console.log(response.status);
        })

    
    
});