app.controller("userHomeController", function ($scope, $http, $location, $rootScope, $routeParams) {
    var req = {
        method: 'GET',
        url: 'http://tracking.in:3000/user/getProducts',

    }
    console.log(req);
    $http(req).then(function successCallback(response) {
        $scope.products = response.data;



    }, function errorCallback(response) {
        $scope.errorStatus = response.status;
        console.log(response.status);
    })

});

app.controller("placeOrderController", function ($scope, $http, $location, $rootScope, $routeParams) {
    var req = {
        method: 'GET',
        url: 'http://tracking.in:3000/user/getProduct/' + $routeParams.productId,

    }
    console.log(req);
    $http(req).then(function successCallback(response) {
        $scope.product = response.data;
        console.log($scope.product)


    }, function errorCallback(response) {
        $scope.errorStatus = response.status;
        console.log(response.status);
    })

    var req1 = {
        method: 'GET',
        url: 'http://tracking.in:3000/user/getUser',

    }
    console.log(req);
    $http(req1).then(function successCallback(response) {
        $scope.user = response.data;
        console.log($scope.user)


    }, function errorCallback(response) {
        $scope.errorStatus = response.status;
        console.log(response.status);
    })

    $scope.placeOrder = function () {
        var req2 = {
            method: 'POST',
            url: 'http://tracking.in:3000/user/placeOrder',
            data: { productId: $routeParams.productId }

        }
        console.log(req2);
        $http(req2).then(function successCallback(response) {
            console.log(response.data)
            $location.path("/user/continueShopping")

        }, function errorCallback(response) {
            $scope.errorStatus = response.status;
            console.log(response.status);
        })

    }

});
app.controller("trackingController", function ($scope, $http, $location, $rootScope, $routeParams) {
    $scope.inPlace = "btn btn-primary"
    var req = {
        method: 'GET',
        url: 'http://tracking.in:3000/trackOrder',

    }
    console.log(req);
    $http(req).then(function successCallback(response) {
        $scope.order = response.data;
        console.log($scope.order);
        
        



    }, function errorCallback(response) {
        $scope.errorStatus = response.status;
        console.log(response.status);
    })

});

app.controller("continueShoppingController", function ($scope, $http, $location, $rootScope, $routeParams) {
    $scope.continueShopping = function () {
        $location.path('/user/home');
    }

});