var Hapi = require('hapi');

const Routes = require('./routes/routes.js');
const HapiSwagger = require('hapi-swagger');
const Pack = require('./package');
const Vision = require('vision');
const Inert = require('inert');
var r = require('rethinkdb');
var DataAccess = require('./dataAccess/table.js')
var thinky = require('./dataAccess/thinky.js')
var Cookie = require('hapi-auth-cookie');

const server = new Hapi.Server();
server.connection({
    host: (process.env.HOST || 'localhost'),
    port: (process.env.PORT || 3000),
    routes: { cors: true, }
});
const options = {
    info: {
        'title': 'Test API Documentation',
        'version': Pack.version,
    }
};






server.register([
    Cookie,
    Inert,
    Vision,
    {
        'register': HapiSwagger,
        'options': options
    }], (err) => {
        server.auth.strategy('standard', 'cookie', {
            password: 'abcdefghijklmnopqrstuvwxyzabcdef',
            cookie: 'maincookie',
            isSecure : false,
            ttl: 60000000,
        });
        server.route(Routes);
        server.start((err) => {
            if (err) {
                console.log(err);
            } else {


            }
        });
    });

server.start();

module.exports = server;
