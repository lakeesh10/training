
'use strict'
var Joi = require('joi');
var userHandler = require('../handler/userHandler.js');
var productHandler = require('../handler/productHandler.js')
var orderHandler = require('../handler/orderHandler.js')
var _ = require('lodash');
var schema = require('../schema/trackingSchema.js')
let userRegistration = {
    method: 'POST',
    path: '/user/register',
    handler: (request, reply) => {
        console.log(request.payload);
        userHandler.registerUser(request.payload, reply);
    },
    config: {
        cors: {
            origin: ['*'],

            credentials: true,
        },
        tags: ['api'],
        validate: {
            payload : schema.registrationSchema
        }

    }
};
let userLogin = {
    method: 'POST',
    path: '/user/login',
    handler: (request, reply) => {
        console.log(request.payload);
        userHandler.authoriseUser(request, request.payload.userId, request.payload.password, request.payload.isAdmin, reply);



    },
    config: {

        cors: {
            origin: ['*'],
            additionalHeaders: ['userid'],
            credentials: true,
        },
        tags: ['api'],
        validate : {
            payload: schema.loginSchema
        }

    }
}
let userLogout = {
    method: 'GET',
    path: '/user/logout',
    handler: (request, reply) => {
        request.cookieAuth.clear();
        reply("Success");
    },
    config: {
        cors: {
            origin: ['*'],
            additionalHeaders: ['userid'],
            credentials: true,
        },
        tags: ['api'],

    }
}
let addProduct = {
    method: 'POST',
    path: '/admin/addProduct',
    handler: (request, reply) => {
        let productObject = request.payload;
        productHandler.addProduct(productObject, request.auth.credentials.userId, reply)
    },
    config: {
        auth: 'standard',
        cors: {
            origin: ['*'],

            credentials: true,

        },
        tags: ['api'],
        validate:{
            payload: schema.addProductSchema
        }

    }
}
let getProducts = {
    method: 'GET',
    path: '/user/getProducts',
    handler: (request, reply) => {
        productHandler.getProducts(reply)
    },
    config: {
        auth: 'standard',
        cors: {
            origin: ['*'],

            credentials: true,

        },
        tags: ['api'],

    }
}
let getUser = {
    method: 'GET',
    path: '/user/getUser',
    handler: (request, reply) => {
        let userId = request.auth.credentials.userId;
        userHandler.getUser(userId,reply)
    },
    config: {
        auth: 'standard',
        cors: {
            origin: ['*'],

            credentials: true,

        },
        tags: ['api'],

    }
}
let getProduct = {
    method: 'GET',
    path: '/user/getProduct/{productId}',
    handler: (request, reply) => {
        productHandler.getProduct(request.params.productId,reply)
    },
    config: {
        auth: 'standard',
        cors: {
            origin: ['*'],

            credentials: true,

        },
        tags: ['api'],
        validate:{
            params: schema.productId
        }

    }
}
let placeOrder = {
    method: 'POST',
    path: '/user/placeOrder',
    handler: (request, reply) => {
        let userId = request.auth.credentials.userId;
        let orderObject = {};
        orderObject.userId = userId;
        orderObject.productId = request.payload.productId;
        orderHandler.placeOrder(orderObject, reply)
    },
    config: {
        auth: 'standard',
        cors: {
            origin: ['*'],

            credentials: true,
        },
        tags: ['api'],
        validate: {
            payload : schema.productId
        }

    }
}
let trackOrder = {
    method: 'get',
    path: '/trackOrder/{orderId}',
    handler: (request, reply) => {
        let orderId = request.params.orderId;
        orderHandler.trackOrderById(orderId, reply)
    },
    config: {
        cors: {
            origin: ['*'],

            credentials: true,
        },
        tags: ['api'],
        validate: {
            params : schema.orderId
        }

    }
}
let trackUserOrder = {
    method: 'get',
    path: '/trackOrder',
    handler: (request, reply) => {
        let userId = request.auth.credentials.userId;
        orderHandler.trackOrderByUser(userId, reply);
    },
    config: {
        auth: 'standard',
        cors: {
            origin: ['*'],

            credentials: true,
        },
        tags: ['api'],

    }
}

let orderReached = {
    method: 'post',
    path: '/admin/reached',
    handler: (request, reply) => {
        let userId = request.auth.credentials.userId;
        orderHandler.orderReached(userId, request.payload.bundleId, reply);
    },
    config: {
        auth: 'standard',
        cors: {
            origin: ['*'],
            credentials: true,
        },
        tags: ['api'],
        validate : {
            payload : schema.bundleId
        }

    }
}

let addToBundle = {
    method: 'post',
    path: '/admin/addBundle',
    handler: (request, reply) => {
        let userId = request.auth.credentials.userId;
        orderHandler.addOrderToBundle(request.payload.bundles, userId, request.payload.toLocation, reply);
    },
    config: {
        auth: 'standard',
        cors: {
            origin: ['*'],

            credentials: true,
        },
        tags: ['api'],
        validate: {
            payload : schema.addToBundleSchema
        }

    }
}

let getRecieved = {
    method: 'get',
    path: '/admin/getRecieved',
    handler: (request, reply) => {
        let userId = request.auth.credentials.userId;
        orderHandler.getBundlesReached(userId, reply);
    },
    config: {
        auth: 'standard',
        cors: {
            origin: ['*'],

            credentials: true,
        },
        tags: ['api'],

    }
}
let getTransit = {
    method: 'get',
    path: '/admin/getTransit',
    handler: (request, reply) => {
        console.log(request.auth.credentials.userId);
        let userId = request.auth.credentials.userId;
        orderHandler.getBundlesTransit(userId, reply);
    },
    config: {
        auth: 'standard',
        cors: {
            origin: ['*'],
            credentials: true,
        },
        tags: ['api'],

    }
}
let getTransported = {
    method: 'get',
    path: '/admin/getTransported',
    handler: (request, reply) => {
        let userId = request.auth.credentials.userId;
        orderHandler.getBundlesTransported(userId, reply);
    },
    config: {
        auth: 'standard',
        cors: {
            origin: ['*'],

            credentials: true,
        },
        tags: ['api'],

    }
}
let getBundle = {
    method: 'post',
    path: '/admin/getBundle',
    handler: (request, reply) => {
        let userId = request.auth.credentials.userId;
        orderHandler.getBundle(request.payload.bundleId, reply);
    },
    config: {
        auth: 'standard',
        cors: {
            origin: ['*'],

            credentials: true,
        },
        tags: ['api'],
        
    }
}
let getDelivered = {
    method: 'get',
    path: '/admin/getDelivered',
    handler: (request, reply) => {
        let userId = request.auth.credentials.userId;
        orderHandler.getOrderForDelivery(userId, reply);
    },
    config: {
        auth: 'standard',
        cors: {
            origin: ['*'],
            credentials: true,
        },
        tags: ['api'],

    }
}
let deliver = {
    method: 'put',
    path: '/admin/deliver',
    handler: (request, reply) => {
        let userId = request.auth.credentials.userId;
        orderHandler.deliverOrder(request.payload.orderId, reply);
    },
    config: {
        auth: 'standard',
        cors: {
            origin: ['*'],
            credentials: true,
        },
        tags: ['api'],
        validate : {
            payload: schema.orderId
        }

    }
}
module.exports = [
    userRegistration,
    userLogin,
    addProduct,
    placeOrder,
    trackOrder,
    trackUserOrder,
    orderReached,
    addToBundle,
    getDelivered,
    getRecieved,
    getTransported,
    getTransit,
    deliver,
    userLogout,
    getProducts,
    getProduct,
    getUser,
    getBundle
];

