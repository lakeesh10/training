var app = angular.module('my-app', [])
app.controller('MainController', function($scope, $rootScope, $http) {
 $scope.snippetId="";

 
 $scope.put = function(){
    var headers={}
    headers.xid = $scope.userId;
    var data={}
    data.language=$scope.language;
    data.codeSnippet=$scope.code;
    if(!$scope.snippetId)
         $scope.errorStatus="Enter Snippet Id"
    else{    
        var req = {
            method: 'PUT',
            url: 'http://localhost:3000/snippet/'+$scope.snippetId,
            headers: headers,
            data: data
        }
        console.log(req);
        $http(req).then(function successCallback(response) {
            
            $scope.response=response.data;
            console.log(response)
            $scope.status=response.status;
        }, function errorCallback(response){
            $scope.errorStatus=response.status;
            console.log(response.status); 
        })
    }
 }
 $scope.post = function(){
    var headers={}
    headers.xid = $scope.userId;
    var data={}
    data.language=$scope.language;
    data.codeSnippet=$scope.code;
    if($scope.snippetId)
         $scope.errorStatus="Snippet Id is not User Defined for POST"
    else{    
        var req = {
            method: 'POST',
            url: 'http://localhost:3000/snippet/',
            headers: headers,
            data: data
        }
        console.log(req);
        $http(req).then(function successCallback(response) {
            
            $scope.response=response.data;
            console.log(response.data)
            $scope.status=response.status;
            $scope.errorStatus="";
        }, function errorCallback(response){
            $scope.errorStatus=response.status;
            console.log(response.status); 
        })
    }
 }
 $scope.delete = function(){
     $scope.response=[]
    var headers={}
    headers.xid = $scope.userId;
    if(!$scope.snippetId)
         $scope.errorStatus="Snippet Id is necessary for DELETE"
    else{    
        var req = {
            method: 'DELETE',
            url: 'http://localhost:3000/snippet/'+$scope.snippetId,
            headers: headers,
        }
        console.log(req);
        $http(req).then(function successCallback(response) {
            $scope.response=response.data;
            console.log(response)
            $scope.status=response.status;
        }, function errorCallback(response){
            $scope.errorStatus=response.status;
            console.log(response.status); 
        })
    }
 }
$scope.get = function(){
    var headers={}
    headers.xid = $scope.userId;
    if($scope.snippetId){
        var id=$scope.snippetId;
        var req = {
            method: 'GET',
            url: 'http://localhost:3000/snippet/'+id,
            headers: headers
        }
        console.log(req);
        $http(req).then(function successCallback(response) {
            $scope.response = response.data ;
            console.log($scope.response)
            $scope.status=response.status;
        }, function errorCallback(response){
            $scope.errorStatus=response.status;
            console.log(response.status); 
        })
            
        
    }
    else {   
        var req = {
            method: 'GET',
            url: 'http://localhost:3000/snippet/',
            headers: headers
        }
        console.log(req);
        $http(req).then(function successCallback(response) {
            
            $scope.response=response.data;
            console.log(response)
            $scope.status=response.status;
        }, function errorCallback(response){
            $scope.errorStatus=response.status;
            console.log(response.status); 
        })
    }
}
});