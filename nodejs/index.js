var Hapi = require('hapi');
const Routes = require('./routes/routes.js');
const HapiSwagger = require('hapi-swagger');
const Pack = require('./package');
const Vision = require('vision');
const Inert = require('inert');
var r = require('rethinkdb');
var DataAccess = require('./handler/dataAccess.js')


var db = new DataAccess();
db.initConnection();

const server = new Hapi.Server();
server.connection({
    host: (process.env.HOST || 'localhost'),
    port: (process.env.PORT || 3000),
    routes: { cors: true, }
});
const options = {
    info: {
        'title': 'Test API Documentation',
        'version': Pack.version,
    }
};

server.register([
    Inert,
    Vision,
    {
        'register': HapiSwagger,
        'options': options
    }], (err) => {
        server.start((err) => {
            if (err) {
                console.log(err);
            } else {
                console.log('Server running at:', server.info.uri);
            }
        });
    });
server.route(Routes);

server.start();