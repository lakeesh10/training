var fs = require("fs");
var outputFilename = './routes/snippit.json';
findObject = (objectArray, value) => {
  for (var j = 0; j < objectArray.length; j++) {

    if (objectArray[j].id == value)
      return objectArray[j];
  }
  return null;
}
updateObject = (objectArray, value, object) => {
  for (var j = 0; j < objectArray.length; j++) {

    if (objectArray[j].id == value) {
      objectArray[j].codeSnippet = object.codeSnippet;
      objectArray[j].language = object.language;
      objectArray[j].dateModified = timeStramp();
      return objectArray;
    }
  }
  return null;
}
removeObject = (objectArray, value) => {
  for (var j = 0; j < objectArray.length; j++) {

    if (objectArray[j].id == value)
      objectArray.splice(j, 1);
  }
  return objectArray;
}

getJSONFile = (filename) => {
  console.log(filename);
  var contents = fs.readFileSync(filename);
  var JSONContent = JSON.parse(contents);
  return JSONContent;
}
randomInt = (low, high) => {
  return Math.floor(Math.random() * (high - low) + low);
}

timeStramp = () => {
  return Date();
}
module.exports = {
  findObject,
  removeObject,
  getJSONFile,
  randomInt,
  timeStramp,
  updateObject
}