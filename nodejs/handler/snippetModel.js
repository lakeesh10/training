'use strict'

var fs = require("fs");
let common = require('../handler/common.js')
let DataAccess = require('../handler/dataAccess.js')
var _count = 0;
class SnippetModel {

    constructor() {

    }

    getSnippetById(userId, snippetId, reply) {
        let configFile = common.getJSONFile("./config/config.json");
        if (!common.findObject(configFile, userId))
            reply("Ne User Illa da").code(401);
        else {
            var db = new DataAccess();
            db.initConnection();
            db.getSnippetById(userId, snippetId).then((result) => {
                console.log(result);
                reply(result).code(200);
            }, (err) => {
                reply("Invalid Arguements").code(404);
                console.log(err);
            });

        }

    }

    getSnippet(userId, reply) {
        let configFile = common.getJSONFile("./config/config.json");
        if (!common.findObject(configFile, userId))
            reply("Ne User Illa da").code(401);
        else {
            var db = new DataAccess();
            db.initConnection();
            db.getSnippet(userId).then((result) => {
                console.log(result);
                reply(result).code(200);
            }, (err) => {
                reply("Invalid Arguements").code(404)
                console.log(err);
            });

        }

    }

    addSnippet(userId, codeSnippet, reply) {

        let configFile = common.getJSONFile("./config/config.json");
        if (!common.findObject(configFile, userId))
            reply("Ne User Illa da").code(401);
        else {
            let object;
            var db = new DataAccess();
            db.initConnection();
            db.getSnippet(userId).then((result) => {
                console.log(result);
                object = result;
                console.log(object);
                codeSnippet.snippetId = common.randomInt(0, 100);
                while (common.findObject(object, codeSnippet.snippetId))
                    codeSnippet.snippetId = common.randomInt(0, 100);
                codeSnippet.snippetId = codeSnippet.snippetId.toString();
                codeSnippet.userId = userId;
                codeSnippet.dateCreated = common.timeStramp();
                codeSnippet.dateModified = common.timeStramp();
                db.insertSnippet(codeSnippet).then((result) => {
                    console.log(result);
                    reply(result).code(200);
                }, (err) => {
                    reply("Invalid Arguements").code(404)
                    console.log(err);
                });
            }, (err) => {
                console.log(err);
            });

        }

    }
    deleteSnippet(userId, snippetId, reply) {
        let configFile = common.getJSONFile("./config/config.json");
        if (!common.findObject(configFile, userId))
            reply("Ne User Illa da").code(401);
        else {
            var db = new DataAccess();
            db.initConnection();
            db.deleteSnippet(userId, snippetId);
            reply("Deleted").code(200);
        }
    }
    updateSnippet(userId, snippetId, codeSnippet, reply) {
        let configFile = common.getJSONFile("./config/config.json");
        if (!common.findObject(configFile, userId))
            reply("Ne User Illa da").code(401);
        else {
            codeSnippet.userId = userId;
            codeSnippet.dateModified = common.timeStramp();
            var db = new DataAccess();
            db.initConnection();
            db.updateSnippet(userId, snippetId, codeSnippet).then((result)=>{;
            reply(result).code(200);
            },(err)=>{
                reply("Invalid Arguements").code(404)
            })

        }
    }


}

module.exports = SnippetModel;