'use strict'

var r = require('reThinkDb');
var co = require('co');
var _conn;
var joi = require('joi');
var dbSchema = require('../schema/dbschema.js')
class DataAccess {
    constructor() {
        this.conn = _conn;
    }
    insertSnippet(object) {
        let me = this;
        return co(function* () {
            try {
                joi.validate(object, dbSchema.postSchema, (error, value) => {
                    if (error) {
                        console.log(error);
                        throw (error);
                    }
                })
                //console.log(_conn);
                console.log("In insertConnection");
                let value = yield r.table('Users').insert(object).run(me.conn);
                return [object];
            }
            catch (err) {
                console.log(err);
                throw (err);

            }
        })
    }
    updateSnippet(userId, snippetId, object) {
        let me = this;
        return co(function* () {
            try {
                console.log(object);
                joi.validate(object, dbSchema.putSchema, (error, value) => {
                    if (error){
                        console.log(error);
                        throw error;
                    }
                })
                console.log(userId, snippetId);
                console.log("In updateSnippet");
                let x=yield r.table('Users').filter({ 'userId': userId, 'snippetId': snippetId }).update({ "codeSnippet": object.codeSnippet, "dateModified": object.dateModified, "language": object.language }).run(me.conn);
                let cursor = yield r.table('Users').filter({ 'snippetId': snippetId, 'userId': userId }).run(me.conn);
                let res = yield cursor.toArray();
                return res;
            }
            catch (err) {
                console.log(err);
                throw error;
                
            }
        })
    }
    deleteSnippet(userId, snippetId) {
        let me = this;
        return co(function* () {
            try {
                joi.validate({ 'userId': userId, 'snippetId': snippetId }, dbSchema.deleteSchema, (error, value) => {
                    if (error)
                        throw error;
                })
                console.log(userId, snippetId);
                console.log("In deleteSnippet");
                let x = yield r.table('Users').filter({ 'userId': userId, 'snippetId': snippetId }).delete().run(me.conn);
                
                return res;
            }
            catch (err) {
                console.log(err);
            }
        })
    }
    getSnippetById(userId, snippetId) {
        let me = this;
        return co(function* () {
            try {
                joi.validate({ 'userId': userId, 'snippetId': snippetId }, dbSchema.getSchemaById, (error, value) => {
                    if (error)
                        throw error;
                })
                console.log(userId, snippetId);
                console.log("In getSnippetById");
                let cursor = yield r.table('Users').filter({ 'snippetId': snippetId, 'userId': userId }).run(me.conn);
                let res = yield cursor.toArray();
                //console.log(res);
                return res;
            }
            catch (err) {
                console.log(err);
            }
        })
    }
    getSnippet(userId) {
        let me = this;
        return co(function* () {
            try {
                joi.validate(userId, dbSchema.getSchema, (error, value) => {
                    if (error)
                        throw error;
                })
                //console.log(userId, snippetId);
                console.log("In getSnippetById");
                let cursor = yield r.table('Users').filter({ 'userId': userId }).run(me.conn);
                let res = yield cursor.toArray();
                console.log(res);
                return res;
            }
            catch (err) {
                console.log(err);
            }
        })
    }
    initConnection() {
        var me = this;
        return co(function* () {
            try {
                if (!_conn) {
                    _conn = yield r.connect({ host: 'localhost', port: 28015, db: 'SnippetDatabase' });
                    console.log('Set _conn');
                    me.conn = _conn;
                }
                //console.log(_conn)
            }
            catch (err) {
                console.log(err);
            }
        })
    }






}


module.exports = DataAccess;