var joi = require('joi');

const getSchema = joi.string().required();


const postSchema = joi.object({
    'codeSnippet': joi.string().required(),
    'language': joi.string().valid(['c', 'c++', 'java', 'js']).required(),
    'dateCreated': joi.string().required(),
    'dateModified': joi.string().required(),
    'userId': joi.string().required(),
    'snippetId': joi.string().required()
});
const putSchema = joi.object({
    'codeSnippet': joi.string().required(),
    'language': joi.string().valid(['c', 'c++', 'java', 'js']).required(),
    'dateModified': joi.string().required(),
    'userId': joi.string().required(),
});

const deleteSchema = joi.object({
    'userId': joi.string().required(),
    'snippetId': joi.string().required()
});
const getSchemaById = joi.object({
    'userId': joi.string().required(),
    'snippetId': joi.string().required()
});
module.exports = {
    postSchema,
    putSchema,
    getSchema,
    getSchemaById,
    deleteSchema
};