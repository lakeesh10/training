var joi = require('joi');

const headers = joi.object({
  'xid': joi.string().required()
}).unknown(true);

const postSchema = joi.object({
  'codeSnippet': joi.string().required(),
  'language': joi.string().valid(['c', 'c++', 'java', 'js']).required()
});
const paramId = joi.object({
  'snippetId': joi.string().required()
});
module.exports = {
  headers,
  postSchema,
  paramId
};