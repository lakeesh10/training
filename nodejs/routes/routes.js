
snippet = require('../handler/snippetModel.js')
schema = require('../schema/schema.js')
var Joi = require('joi');

module.exports = [{
    method: 'GET',
    path: '/snippet/{snippetId}',
    handler: (request, reply) => {
        snippetModel = new snippet();
        snippetModel.getSnippetById(request.headers.xid, request.params.snippetId, reply);
    },
    config: {
        cors: {
            origin: ['*'],
            additionalHeaders: ['xid']
        },
        tags: ['api'],
        validate: {
            headers: schema.headers
        }
    }
},
    {
        method: 'GET',
        path: '/snippet/',
        handler: (request, reply) => {
            snippetModel = new snippet();
            snippetModel.getSnippet(request.headers.xid, reply);
        },
        config: {
            cors: {
                origin: ['*'],
                additionalHeaders: ['xid']
            },
            tags: ['api'],
            validate:
            {
                headers: schema.headers
            }
        }
    },
    {
        method: 'POST',
        path: '/snippet/',
        handler: (request, reply) => {
            console.log(request.payload);
            snippetModel = new snippet();
            snippetModel.addSnippet(request.headers.xid, request.payload, reply)
        },
        config: {
            cors: {
                origin: ['*'],
                additionalHeaders: ['xid']
            },
            tags: ['api'],
            validate: {

                headers: schema.headers,
                payload: schema.postSchema
            }

        }
    },
    {
        method: 'PUT',
        path: '/snippet/{snippetId}',
        handler: (request, reply) => {
            console.log(request.payload);
            snippetModel = new snippet();
            snippetModel.updateSnippet(request.headers.xid, request.params.snippetId, request.payload, reply)
        },
        config: {
            cors: {
                origin: ['*'],
                additionalHeaders: ['xid']
            },
            tags: ['api'],
            validate: {

                headers: schema.headers,
                payload: schema.postSchema,
                params: schema.paramId
            }

        }
    },
    {
        method: 'DELETE',
        path: '/snippet/{snippetId}',
        handler: (request, reply) => {
            console.log(request);
            snippetModel = new snippet();
            snippetModel.deleteSnippet(request.headers.xid, request.params.snippetId, reply)
        },
        config: {
            cors: {
                origin: ['*'],
                additionalHeaders: ['xid']
            },
            tags: ['api'],
            validate:
            {
                headers: schema.headers,
                params: schema.paramId
            }

        }
    }
];

